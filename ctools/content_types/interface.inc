<?php

/**
 * @file
 * Exposes the UberPOS interface and allows it to accept the order_id as an
 * argument.
 */

/**
 * Callback function to supply a list of content types.
 */
function uberpos_interface_ctools_content_types() {
  return array(
    'title' => t('UberPOS Screen'),
    'defaults' => array(
      'args' => '',
    ),
    'all contexts' => TRUE,
  );
}

/**
 * Return the interface info.
 */
function uberpos_interface_content_type_content_types() {
  return array(
    array(
      'title' => t('UberPOS Interface'),
      'category' => t('UberPOS Screen'),
    ),
  );
}

/**
 * Renders the interface, loading the order if provided.
 */
function uberpos_interface_content_type_render($subtype, $conf) {
  $arguments = explode('/', $_GET['q']);
  $args = $conf['args'];

  foreach ($arguments as $id => $arg) {
    $args = str_replace("%$id", $arg, $args);
  }

  $args = preg_replace(',/?(%\d|@\d),', '', $args);
  $args = $args ? explode('/', $args) : array();

  $order = NULL;
  if (is_numeric($args[0])) {
    $order->order_id = $args[0];
  }

  $block->content = theme('uberpos_screen_interface', $order);
  return $block;
}

/**
 * Edit form has override title and argument.
 */
function uberpos_interface_content_type_edit_form(&$form, &$form_state) {
  $conf = $form_state['conf'];

  $form['args'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['args'],
    '#title' => t('Order Id Argument'),
    '#size' => 30,
    '#description' => t('The order id may be sent as an argument. You may use %0, %1, ... Only one argument is accepted.'),
  );

  return $form;
}

/**
 * This just dumps everything into the $conf array.
 */
function uberpos_interface_content_type_edit_form_submit(&$form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }

}

/**
 * Returns the administrative title for a type.
 */
function uberpos_interface_content_type_admin_title($subtype, $conf) {
  return t('UberPOS interface');
}

function uberpos_interface_content_type_admin_info($subtype, $conf) {
  $block->title = t('UberPOS interface');
  return $block;
}
