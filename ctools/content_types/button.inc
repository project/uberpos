<?php

/**
 * @file
 * Get all UberPOS buttons and expose them to panels!!!
 */

/**
 * Callback function to supply a list of content types.
 */
function uberpos_button_ctools_content_types() {
  return array(
    'title' => t('UberPOS Buttons'),
    'defaults' => array(
      'args' => '',
    ),
    'all contexts' => TRUE,
  );
}

/**
 * Return all block content types available.
 */
function uberpos_button_content_type_content_types() {
  $types = array();

  $buttons = uberpos_get_buttons();

  foreach ($buttons as $button) {
    $types[$button['text']] = array(
      'title' => $button['text'],
      'category' => t('UberPOS Buttons'),
    );
  }

  return $types;
}

/**
 * Output function for the 'block' content type. Outputs a block
 * based on the module and delta supplied in the configuration.
 */
function uberpos_button_content_type_render($subtype, $conf) {
  $buttons = uberpos_get_buttons();

  foreach ($buttons as $button) {
    if ($button['text'] == $subtype) {
      $block->content = theme('uberpos_screen_button', $button);
      return $block;
    }
  }
}

/**
 * Empty form so we can have the default override title.
 */
function uberpos_button_content_type_edit_form(&$form, &$form_state) {
}

/**
 * Returns the administrative title for a type.
 */
function uberpos_button_content_type_admin_title($subtype, $conf) {
  return t('UberPOS button') .': '. $subtype;
}

/**
 * Give a little preview for after they add it to a pane in the admin interface.
 */
function uberpos_button_content_type_admin_info($subtype, $conf) {
  $buttons = uberpos_get_buttons();

  foreach ($buttons as $button) {
    if ($button['text'] == $subtype) {
      $block->content = theme('uberpos_screen_button', $button);
      return $block;
    }
  }
}
