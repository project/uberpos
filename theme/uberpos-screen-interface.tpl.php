<?php

/**
 * @file
 * Default uberpos module interface template
 */
?>

<div id="uberpos-interface">
  <table id="input-date">
    <tbody>
      <tr>
        <td><?php print $output['input'] ?></td>
        <td class="right date">
          <div id="uberpos-div-throbber"> </div>
          <div id="uberpos-date"> </div>
        </td>
      </tr>
      <tr>
        <td>CUSTOMER <span class='customer'></span> </td>
        <td class="right">CASHIER <?php print $output['cashier']; ?></td>
      </tr>
    </tbody>
   </table>

  <div id="screen">
    <div id="clear-message-container"></div>
    <div id="welcome-message"></div>
    <table id="main-table">
      <tbody>
      </tbody>
    </table>
  </div>

  <div id="line-items"></div>
  <div id="receipt"></div>
</div>

