<?php

/**
 * @file
 * Default uberpos module buttons right template
 */

foreach ($buttons as $button) {
  if ($button['section'] == 'right') {
    print theme('uberpos_screen_button', $button);
  }
}
