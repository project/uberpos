<?php

/**
 * @file
 * Default uberpos module buttons left template
 */

foreach ($buttons as $button) {
  if ($button['section'] == 'left') {
    print theme('uberpos_screen_button', $button);
  }
}
