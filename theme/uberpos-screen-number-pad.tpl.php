<?php

/**
 * @file
 * Default uberpos module number pad template
 */

print '<table id="number-pad"><tr>';
$i = 0;
foreach ($buttons as $button) {
  if ($button['section'] == 'number-pad') {
    $i++;
    if ($i > 1 && (($i - 1) % 3 == 0)) {
      print '</tr><tr>';
    }

    print '<td>';
    if (!empty($button['formatted-help'])) {
      print $button['formatted-help'];
    }
    print '<div ' . drupal_attributes($button['attributes']) .'>';
    print $button['text'];
    print '</div></td>';
  }
}
print "</tr></table>";
