<?php

/**
 * @file
 * Default uberpos module button template
 */

if (!empty($button['formatted-help'])) {
  print $button['formatted-help'];
}
print '<div ' . drupal_attributes($button['attributes']) .'>';
print $button['text'];
print '</div>';
