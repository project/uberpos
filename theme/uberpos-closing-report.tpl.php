<?php
/**
 * @file
 * Default UberPOS Manager report template
 *
 * Available Variables: (All available as keys of $output)
 * title
 * store
 * start_date
 * end_date
 * total_revenue
 * payments_summary - array of type => amount
 * payments - array of date, order_id, method and amount
 * refunds - array of time, title, price, order_id and created
 *
 */
?>
<div class="receipt">
    <h1><?php print $output['title']; ?></h1>
    <?php if ($output['store']):?>
        <h2>Store: <?php print $output['store']; ?></h2>
    <?php endif ?>
    <div class="start-date"><?php print t('Start date:') .' '. format_date($output['start_date']);?></div>
    <div class="end-date"><?php print t('End date:') .' '. format_date($output['end_date']); ?></div>
    <span class="total-revenue"><?php print t('Total Revenue:') .' '. $output['total_revenue']; ?></span>
    <h2><?php print t('Payments Summary'); ?></h2>
        <?php if ($output['payments_summary']) : ?>
        <?php foreach ($output['payments_summary'] as $type => $amount) : ?>
            <div class="payment-summary">
                <span class="payment-method"><?php print $type; ?></span>
                <span class="payment-amount"><?php print $amount; ?></span>
            </div>
        <?php endforeach; ?>
        <?php endif ?>

    <h2><?php print t('Payments'); ?></h2>
    <?php if ($output['payments']) : ?>
    <?php foreach ($output['payments'] as $payment): ?>
        <div class="payment">
            <span class="payment-date"><?php print $payment['date']; ?></span>
            <span class="payment-order-id"><?php print $payment['order_id']; ?></span>
            <span class="payment-method"><?php print $payment['method']; ?></span>
            <span class="payment-amount"><?php print $payment['amount']; ?></span>
        </div>
    <?php endforeach ?>
    <?php endif ?>

    <h2><?php print t('Refunds'); ?></h2>
    <?php if ($output['refunds']) : ?>
    <?php foreach ($output['refunds'] as $refund) : ?>
        <div class="refund">
            <span class="refund-date"><?php print format_date($refund['time']); ?></span>
            <span class="refund-title"><?php print $refund['title'];?>:</span>
            <span class="refund-amount"><?php print $refund['price']; ?></span>
            <span class="refund-order-id"><?php print t('order:') .' '.  $refund['order_id']; ?></span>
            <span class="refund-order-date"><?php print format_date($refund['created']); ?></span>
        </div>
    <?php endforeach ?>
    <?php else: ?>
        <div class="refund"><?php print t('There were no refunds'); ?></div>
    <?php endif ?>
</div>