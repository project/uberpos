<?php

/**
 * @file
 * Default uberpos module template
 */
?>

<div id="uberpos">
  <table><tbody><tr>
    <td class="uberpos-align-top">
      <?php print $output['interface']; ?>
    </td>
    <td class="uberpos-align-top">
      <table id="buttons"><tbody><tr>
        <td>
          <?php print $output['buttons_num']; ?>
          <?php print $output['buttons_left']; ?>
        </td>
        <td>
          <?php print $output['buttons_right']; ?>
        </td>
      </tr></tbody></table>
    </td>

  </tr></tbody></table>
</div>

