<?php

/**
 * @file
 * Default UberPOS module receipt template
 *
 * Available variables:
 * - $output: Contains the formatted receipt header and footer set in the
 * UberPOS settings, the formatted current time and date, the cashier's
 * username, line items, order id formatted to a barcode image which contains
 * the text '[order id]OR' (only if barcode module is installed). Lastly,
 * $output['screen'] contains a table of the order's products and prices for
 * easy output.
 * - $order: This is the ubercart order. Products are in $order->products.
 *
 * Special note:
 * Anything between a <div class="receipt"> </div> will be printed  on a
 * receipt. If you'd like to print multiple receipts for some reason, include
 * multiple div's with the "receipt" class. Nothing outside of one of these
 * div's will appear on the receipt.
 */
?>

<?php /* TEXT OUTSIDE OF 'receipt' CLASS WILL NOT APPEAR IN RECEIPT */ ?>

<div id="receipt-body" class="receipt">

<?php /* TEXT INSIDE WILL :) */ ?>

<?php
  print '<div id="receipt-header">'. $output['receipt_header'] .'</div>';
  print '<div id="receipt-date">'. $output['date'] .'</div>';
  print '<div id="receipt-screen">'. $output['screen'] .'</div>';

  if (isset($output['line_items'])) {
    foreach ($output['line_items'] as $line_item) {
      print '<div class="uberpos-line-item">'. $line_item['title'] .': '. $line_item['amount'] .'</div>';
    }
  }
  if (isset($order->data['cc_txns']['references'])) {
    foreach ($order->data['cc_txns']['references'] as $txn) {
      print 'Card #: XXXX XXXX XXXX ' . $txn['card'];
    }
  }

  print '<div id="receipt-order-id">'. t('Order ID: @order', array('@order' => $order->order_id)) .'</div>';
  if (isset($output['receipt_barcode'])) {
    print '<div id="receipt-barcode">'. $output['receipt_barcode'] .'</div>';
  }

  print '<div id="receipt-footer">'. $output['receipt_footer'] .'</div>';
?>

</div>
