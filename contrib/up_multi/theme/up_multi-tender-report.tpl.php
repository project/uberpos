<?php
/**
 * @file
 * Tender report template file
 */
?>

<div class="receipt">
<h1><?php print t('Tender Report'); ?></h1>
<div class="cashier"><?php print $output['cashier']; ?></div>
<div class="start-date"><?php print t('Start date:') .' '. format_date($output['start_date']);?></div>
<div class="end-date"><?php print t('End date:') .' '. format_date($output['end_date']); ?></div>
<br />
<h3><?php print t('Totals')?></h3>
<div id="totals">
<?php if ($output['totals']) : ?>
<?php foreach ($output['totals'] as $title => $total): ?>
    <div><b><?php print $title; ?></b>: <?php print $total; ?></div>
<?php endforeach ?>
<?php endif ?>
</div>
<br />
<br />
<div id="payments">
<?php if ($output['payments']) : ?>
<?php foreach ($output['payments'] as $method => $payments):?>
<h3><?php print $method; ?></h3>
<?php foreach ($payments as $payment) : ?>
    <div><?php print $payment['amount']; ?> - Order #<?php print $payment['order_id']; ?> - <?php print $payment['received']; ?></div>
<?php endforeach ?>
<br />
<?php endforeach ?>
<?php else: ?>
    <div><?php print t('There were no payments.'); ?></div>
<?php endif ?>
</div>

<div id="refunds">
<h3>Refunds</h3>
<?php if ($output['refunds']) : ?>
<?php foreach ($output['refunds'] as $refund):?>
    <div><?php print $refund['amount']; ?> - Order #<?php print $refund['order_id']; ?> - <?php print $refund['received']; ?></div>
<?php endforeach ?>
<?php else:  ?>
    <div><?php print t('There were no refunds'); ?></div>
<?php endif ?>
</div>
</div>