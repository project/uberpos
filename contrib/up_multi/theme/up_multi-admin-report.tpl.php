<?php
/**
 * @file
 * Administration report template for Uberpos
 * Variables available:
 * $results
 *  ->gross  - Overall gross amount
 *  ->cost  - Overall cost of products sold
 *  ->net  - Overall net amount
 *  ->refunds  - Overall refunds
 *  ->tax  -  Overall tax
 *  ->displaying - String listing the date being displayed
 *  ->filter  -  Date/Location filter
 *  ->payments  - Listing of payment totals by type.  This is an array.
 *  ->stores -  A themed table listing the details of each store.
 */
?>
<?php print $results->displaying; ?>
<?php print $results->filter; ?>

<fieldset id="totals">
<legend><h3><?php print t('Totals'); ?></h3></legend>
<div class="gross"><?php print t('Gross:'); ?> <?php print $results->gross; ?></div>
<div class="cost"><?php print t('Cost'); ?>: <?php print $results->cost; ?></div>
<div class="net"><?php print t('Net'); ?>: <?php print $results->net; ?></div>

<div class="refunds"><?php print t('Refunds'); ?>: <?php print $results->refunds; ?></div>
<div class="refunds"><?php print t('Tax'); ?>: <?php print $results->tax; ?></div>
</fieldset>

<?php if ($results->payments) : ?>
<fieldset id="payments">
<legend><h3><?php print t('Payments'); ?></h3></legend>
<?php foreach ($results->payments as $type => $amount) : ?>
<div class="pay-row"><?php print $type . ': ' . $amount; ?></div>
<?php endforeach ?>
</fieldset>
<?php endif ?>

<fieldset id="stores">
<legend><h3><?php print t('Stores'); ?></h3></legend>
<?php print $results->stores; ?>
</fieldset>
