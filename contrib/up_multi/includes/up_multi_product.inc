<?php

/**
 * @file
 * This file contains the forms and functions necessary for managing store-specific aspects
 * of products.
 */

/**
 * Retrieve sell price, cost and stock levels for each store.
 *
 * @return
 *  An array with Store IDs as keys and arrays of sell_price, cost and stock_level as values.
 */
function _up_multi_product_data($node) {
  $data = array();

  $r = db_query('SELECT sell_price, cost, store_id FROM {up_store_products} WHERE nid=%d', $node->nid);
  $r2 = db_query("SELECT stock_level, store_id FROM {up_store_stock} WHERE model = '%s'", $node->model);

  while ($row = db_fetch_array($r)) {
    $data[$row['store_id']]['sell_price'] = $row['sell_price'];
    $data[$row['store_id']]['cost'] = $row['cost'];
  }

  while ($row = db_fetch_array($r2)) {
    $data[$row['store_id']]['stock_level'] = $row['stock_level'];
  }

  return $data;
}

function up_multi_store_price_form($form_state, $nid = NULL) {
  $node = node_load($nid);
  $data = _up_multi_product_data($node);
  $form = array('stores' => array('#tree' => TRUE, '#weight' => 1));

  foreach (up_multi_stores() as $store) {
    $form['stores'][$store->store_id] = array(
      'store_name' => array(
        '#value' => $store->store_name,
      ),
      'price' => array(
         '#type' => 'textfield',
         '#size' => 3,
         '#default_value' => isset($data[$store->store_id]['sell_price']) ? uc_store_format_price_field_value($data[$store->store_id]['sell_price']) : 0,
      ),
      'cost' => array(
         '#type' => 'textfield',
         '#size' => 3,
         '#default_value' => isset($data[$store->store_id]['cost']) ? uc_store_format_price_field_value($data[$store->store_id]['cost']) : 0,
      ),
      'stock' => array(
        '#type' => 'textfield',
        '#size' => 3,
        '#default_value' => $data[$store->store_id]['stock_level'],
      ),
    );
  }

  $form['#theme'] = 'store_price_form';
  return $form;
}

function theme_store_price_form($form) {
  drupal_add_js(drupal_get_path('module', 'uberpos') . '/js/stock_adjust.js');

  $header = array(t('Name'), t('Price'), t('Cost'), t('Stock'));
  foreach (element_children($form['stores']) as $store) {
    foreach (element_children($form['stores'][$store]) as $col) {
      $rows[$store][] = drupal_render($form['stores'][$store][$col]);
    }
  }
  $output = theme('table', $header, $rows);
  unset($form['stores']);
  $output .= drupal_render($form);
  return $output;
}

function up_multi_store_price_form_submit($form, &$form_state) {
  $nid = $form['nid']['#value'];
  $product = node_load($nid);

  foreach ($form_state['values']['stores'] as $store => $vals) {
    $price = array('nid' => $nid, 'store_id' => $store, 'sell_price' => $vals['price'], 'cost' => $vals['cost']);
    $stock = array('model' => $product->model, 'store_id' => $store, 'stock_level' => $vals['stock']);

    if ($form['multistore']['stores'][$store]['price']['#default_value'] &&
        $form['multistore']['stores'][$store]['cost']['#default_value']) {
      if ($vals['price'] || $vals['cost']) {
        drupal_write_record('up_store_products', $price, array('store_id', 'nid'));
      }
      else {
        db_query('DELETE FROM {up_store_products} WHERE store_id = %d AND nid = %d', $store, $nid);
      }
    }
    elseif ($vals['price']) {
      drupal_write_record('up_store_products', $price);
    }

    if ($form['multistore']['stores'][$store]['stock']['#default_value']) {
      if ($vals['stock']) {
        drupal_write_record('up_store_stock', $stock, array('store_id', 'model'));
      }
      else {
        db_query('DELETE FROM {up_store_stock} WHERE store_id=%d AND model=%d', $store, $product->model);
      }

    }
    elseif ($vals['stock']) {
      drupal_write_record('up_store_stock', $stock);
    }
  }
}
