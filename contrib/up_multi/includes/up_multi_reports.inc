<?php

/**
 * @file
 * Special reporting functions to generate reports for Uberpos.
 */

/**
 * General admin report.
 */
function up_multi_admin_report($start=NULL, $end=NULL) {
  drupal_add_css(drupal_get_path('module', 'up_multi') .'/theme/up_admin.css');

  if (is_numeric($start)) {
    $args['start'] = $start;
  }
  else {
    $args['start'] = strtotime(date("j F Y", strtotime('-1 month')));
  }
  if (is_numeric($end)) {
    $args['end'] = $end;
  }
  else {
    $args['end'] = strtotime("23:59");
  }

  global $user;
  $ctx = array('account' => $user, 'revision' => 'themed-original');

  $results->gross = db_result(db_query('SELECT SUM(o.order_total) AS gross FROM {uc_orders} o LEFT JOIN {up_orders} upo ON o.order_id = upo.order_id WHERE completed > %d AND completed < %d', $args));
  $results->cost = db_result(db_query('SELECT SUM(cost*qty) AS cost FROM {uc_order_products} op LEFT JOIN {up_orders} o ON op.order_id = o.order_id WHERE completed > %d AND completed < %d', $args));
  $results->refunds = db_result(db_query('SELECT SUM(price) AS refunds FROM {up_log} WHERE command = "RF" AND time > %d AND time < %d', $args));
  $results->tax = db_result(db_query('SELECT SUM(amount) AS tax FROM {up_orders} o RIGHT JOIN {uc_order_line_items} li ON o.order_id = li.order_id WHERE completed > %d AND completed < %d AND type="tax"', $args));
  $payments = db_query('SELECT SUM(amount) amount, method FROM {uc_payment_receipts} p WHERE received > %d AND received < %d GROUP BY method', $args);

  while ($data = db_fetch_array($payments)) {
    $results->payments[$data['method']] = $data['amount'];
  }
  $results->net = $results->gross - $results->cost - $results->tax;
  foreach ($results as &$set) {
    if (is_numeric($set) || $set == NULL) {
      $set = ($set) ? uc_price($set, $ctx) : uc_price(0, $ctx);
    }
    else {
      foreach ($set as &$price) {
        if (is_numeric($price)) {
          $price = uc_price($price, $ctx);
        }
      }
    }
  }
  $headers[] = array('data' => NULL);
  $stores = up_multi_stores();
  $stores[0]->store_name = 'No Store';
  $stores[0]->store_id = 0;
  ksort($stores);
  foreach ($stores as $store) {
    $headers[] = array('data' => check_plain($store->store_name));
  }

  $blank_row = array_fill_keys(array_keys($stores), uc_price(0, $ctx));

  $breakdown->gross = db_query('SELECT SUM(o.order_total) AS gross, store_id FROM {uc_orders} o LEFT JOIN {up_orders} upo ON o.order_id = upo.order_id LEFT JOIN {up_store_orders} s ON o.order_id = s.order_id WHERE completed > %d AND completed < %d GROUP BY store_id', $args);
  $breakdown->cost = db_query('SELECT SUM(cost*qty) AS cost, store_id FROM {uc_order_products} op LEFT JOIN {up_orders} o ON op.order_id = o.order_id LEFT JOIN {up_store_orders} s ON o.order_id = s.order_id WHERE completed > %d AND completed < %d GROUP BY store_id', $args);
  $breakdown->refunds = db_query('SELECT SUM(price) AS refunds, store_id FROM {up_log} l LEFT JOIN {up_orders} o ON l.order_id = o.order_id LEFT JOIN {up_store_orders} s ON o.order_id = s.order_id WHERE command = "RF" AND time > %d AND time < %d GROUP BY store_id', $args);
  $breakdown->tax = db_query('SELECT SUM(amount) AS tax, store_id FROM {uc_order_line_items} li LEFT JOIN {up_orders} upo ON li.order_id = upo.order_id LEFT JOIN {up_store_orders} s ON li.order_id = s.order_id WHERE completed > %d AND completed < %d AND type="tax" GROUP BY store_id', $args);
  $breakdown->payments  = db_query('SELECT SUM(amount) AS amount, method, store_id FROM {uc_payment_receipts} p LEFT JOIN {up_orders} o ON p.order_id = o.order_id LEFT JOIN {up_store_orders} s ON o.order_id = s.order_id WHERE received > %d AND received < %d GROUP BY store_id, method', $args);

  $rows['gross']['title'] = array('class' => 'row-label', 'data' => t('Gross'));
  $rows['cost']['title'] = array('class' => 'row-label', 'data' => t('Cost'));
  $rows['net']['title'] = array('class' => 'row-label', 'data' => t('Net'));
  $rows['refunds']['title'] = array('class' => 'row-label', 'data' => t('Refunds'));
  $rows['tax']['title'] = array('class' => 'row-label', 'data' => t('Tax'));

  foreach ($rows as &$row) {
    $row = array_merge($row, $blank_row);
  }

  while ($data = db_fetch_array($breakdown->gross)) {
    $data['store_id'] = (isset($data['store_id'])) ? $data['store_id'] : 0;
    $rows['gross'][$data['store_id']] = uc_price($data['gross'], $ctx);
    $rows['net'][$data['store_id']] = $data['gross'];
  }
  while ($data = db_fetch_array($breakdown->cost)) {
    $data['store_id'] = (isset($data['store_id'])) ? $data['store_id'] : 0;
    $rows['cost'][$data['store_id']] = uc_price($data['cost'], $ctx);
    $rows['net'][$data['store_id']] -= $data['cost'];
  }
  while ($data = db_fetch_array($breakdown->tax)) {
    $data['store_id'] = (isset($data['store_id'])) ? $data['store_id'] : 0;
    $rows['tax'][$data['store_id']] = uc_price($data['tax'], $ctx);
    $rows['net'][$data['store_id']] -= $data['tax'];
  }
  foreach ($rows['net'] as &$location) {
    if (!$location['data']) {
      $location = uc_price($location, $ctx);
    }
  }
  while ($data = db_fetch_array($breakdown->refunds)) {
    $data['store_id'] = (isset($data['store_id'])) ? $data['store_id'] : 0;
    $rows['refunds'][$data['store_id']] = uc_price($data['refunds'], $ctx);
  }

  while ($data = db_fetch_array($breakdown->payments)) {
    $data['store_id'] = (isset($data['store_id'])) ? $data['store_id'] : 0;
    $pmt_rows[$data['method']][$data['store_id']] = uc_price($data['amount'], $ctx);
    $methods[$data['method']] = $data['method'];
  }

  if ($methods) {
    foreach ($methods as $method) {
      $rows[$method] = $blank_row;
      $rows[$method] = array_replace($blank_row, $pmt_rows[$method]);

      array_unshift($rows[$method], $method);
      $rows[$method] = array('data' => $rows[$method]);
      $rows[$method]['class'] = 'payment-method';
    }
  }

  $redirect = 'admin/store/reports/uberpos';
  $results->filter = drupal_get_form('up_multi_report_filter', $args, $redirect);
  $results->stores = theme('table', $headers, $rows);
  return theme('up_multi_admin_report', $results);
}

function up_multi_payment_report($start = NULL, $end = NULL) {
  drupal_add_css(drupal_get_path('module', 'up_multi') .'/theme/up_admin.css');

  if (is_numeric($start)) {
    $args['start'] = $start;
  }
  else {
    $args['start'] = strtotime(date("j F Y", strtotime('-1 month')));
  }
  if (is_numeric($end)) {
    $args['end'] = $end;
  }
  else {
    $args['end'] = strtotime("23:59");
  }

  global $user;
  $ctx = array('account' => $user, 'revision' => 'themed-original');
  $header = array(
  array('data' => t('Date'), 'field' => 'received'),
  array('data' => t('Order ID'), 'field' => 'p.order_id'),
  array('data' => t('Method'), 'field' => 'method', 'sort' => 'asc'),
  array('data' => t('Amount'), 'field' => 'amount'),
  array('data' => t('Location'), 'field' => 'store_id')
  );
  $q = 'SELECT received, p.order_id, method, amount, store_id FROM {uc_payment_receipts} p LEFT JOIN {up_orders} o ON p.order_id = o.order_id LEFT JOIN {up_store_orders} s ON p.order_id = s.order_id WHERE received > %d AND received < %d';
  $payments = pager_query($q . tablesort_sql($header), 20, 0, NULL, $args);
  $stores = up_multi_stores();

  while ($payment = db_fetch_array($payments)) {
    $payment['received'] = format_date($payment['received']);
    $payment['amount'] = uc_price($payment['amount'], $ctx);
    $payment['order_id'] = l($payment['order_id'], 'admin/store/orders/'. $payment['order_id']);
    $payment['store_id'] = ($payment['store_id']) ? check_plain($stores[$payment['store_id']]->store_name) : 'None';
    $rows[] = $payment;
  }

  $redirect = 'admin/store/reports/uberpos/payments';
  $filter = drupal_get_form('up_multi_report_filter', $args, $redirect);
  return $filter . theme('table', $header, $rows) . theme('pager');
}

function up_multi_product_report($start = NULL, $end = NULL, $location = NULL) {
  drupal_add_css(drupal_get_path('module', 'up_multi') .'/theme/up_admin.css');

  if (is_numeric($start)) {
    $args['start'] = $start;
  }
  else {
    $args['start'] = strtotime(date("j F Y", strtotime('-1 month')));
  }
  if (is_numeric($end)) {
    $args['end'] = $end;
  }
  else {
    $args['end'] = strtotime("23:59");
  }

  $args['location'] = (is_numeric($location)) ? $location : 'ALL';
  global $user;
  $ctx = array('account' => $user, 'revision' => 'themed-original');
  $header = array(
  array('data' => t('Title')),
  array('data' => t('Store')),
  array('data' => t('Quantity')),
  array('data' => t('Gross (less tax)')),
  array('data' => t('Net'))
  );
  $q = 'SELECT n.title, store_id, n.nid, SUM(qty) quant, SUM(qty*price) gross, SUM((qty*price)-(qty*cost)) net FROM {uc_order_products} p LEFT JOIN {node} n ON p.nid = n.nid LEFT JOIN {up_orders} o ON p.order_id = o.order_id LEFT JOIN {uc_orders} uco ON o.order_id = uco.order_id LEFT JOIN {up_store_orders} s ON p.order_id = s.order_id WHERE uco.order_status = \'pos_completed\' AND completed > %d AND completed < %d';
  $count_q = "SELECT COUNT(DISTINCT(n.nid)) FROM {uc_order_products} p LEFT JOIN {node} n ON p.nid = n.nid LEFT JOIN {up_orders} o ON p.order_id = o.order_id LEFT JOIN {uc_orders} uco ON o.order_id = uco.order_id LEFT JOIN {up_store_orders} s ON p.order_id = s.order_id WHERE uco.order_status = 'pos_completed' AND completed > %d AND completed < %d";
  if (is_numeric($args['location'])) {
    $q .= ' AND store_id = %d';
    $count_q .= ' AND store_id = %d';
  }

  $q .= ' GROUP BY n.nid, store_id';

  $results = pager_query($q . tablesort_sql($header), 20, 0, $count_q, $args);

  while ($product = db_fetch_array($results)) {
    $products[$product['nid']][$product['store_id']] = $product;
  }
  $stores = up_multi_stores();

  if ($products) {
    foreach ($products as $product) {

      $row_set = array();
      $total = array( 'data' => array('title' => array(), 'store_id' => 'Total', 'quantity' => 0, 'gross' => 0, 'net' => 0), 'class' => 'total-row');
      $total['data']['title'] = array(
          'data' => NULL,
          'rowspan' => count($product)+1);
      foreach ($product as $pr_store) {
        $row = array();
        if (!$total['data']['title']['data']) {
          $total['data']['title']['data'] = l($pr_store['title'], 'node/'. $pr_store['nid']);
        }
        $total['data']['quantity']+=$pr_store['quant'];
        $total['data']['gross']+= $pr_store['gross'];
        $total['data']['net']+= $pr_store['net'];

        $row['store_id'] = ($stores[$pr_store['store_id']]->store_name) ? check_plain($stores[$pr_store['store_id']]->store_name) : t('No Store');
        $row['quantity'] = $pr_store['quant'];
        $row['gross'] = uc_price($pr_store['gross'], $ctx);
        $row['net'] = uc_price($pr_store['net'], $ctx);

        $row_set[] = $row;
      }
      $total['data']['gross'] = uc_price($total['data']['gross'], $ctx);
      $total['data']['net'] = uc_price($total['data']['net'], $ctx);
      $rows[] = $total;

      foreach ($row_set as $row) {
        $rows[] = $row;
      }
    }
  }

  $redirect = 'admin/store/reports/uberpos/products';
  $filter = drupal_get_form('up_multi_report_filter', $args, $redirect);
  return $filter . theme('table', $header, $rows) . theme('pager');
}

function up_multi_report_filter(&$form_state, $arguments = NULL, $redirect) {
  if ($arguments) {
    if ($arguments['start'] > $arguments['end']) {
      drupal_set_message(t('Invalid query dates.'), 'warning');
    }
    $s = _up_multi_stamptoarray($arguments['start']);
    $e = _up_multi_stamptoarray($arguments['end']);

    $mess_args['start'] = format_date($arguments['start']);
    $mess_args['end'] = format_date($arguments['end']);
    $message = 'Displaying data from start to end';
    if ($arguments['location']) {
      $store = up_multi_stores($arguments['location']);
      if ($store) {
        $mess_args['location'] = check_plain($store->store_name);
        $message .= ' for location';
      }
      elseif ($arguments['location'] = 'ALL') {
        $message .= ' for all locations';
      }
    }
  }
  $form = array(
    'container' => array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,

      'start_date' => array(
          '#type' => 'date',
          '#title' => t('Start Date'),
           '#default_value' => $s,
            '#return_value' => $arguments['start'],
      ),
      'end_date' => array(
          '#type' => 'date',
          '#title' => t('End Date'),
          '#default_value' => $e,
        '#return_value' => $arguments['end'],
  ),
      'submit' => array(
        '#type' => 'submit',
        '#value' => 'Submit',
  ),
      'redirect' => array(
        '#type' => 'value',
        '#value' => $redirect,
  ),
  ),
  );
  if ($arguments['location']) {
    $options['ALL'] = 'Show All';
    foreach (up_multi_stores() as $store) {
      $options[$store->store_id] = $store->store_name;
    }
    $form['container']['location'] = array(
        '#type' => 'select',
        '#title' => 'Location',
        '#options' => $options,
        '#default_value' => $arguments['location'],
    );
  }

  return $form;
}




/**
 * Utility method for converting a Unix timestamp to a
 * date array capable of being parsed by
 * Drupal's date form element.
 * @param $stamp
 */
function _up_multi_stamptoarray($stamp) {
  $timezone = variable_get('date_default_timezone', 0);
  $timezone_offset = time() + $timezone;
  $d = date('Y-n-j', $stamp-$timezone);
  $d = explode('-', $d);
  $d1['year'] = $d[0];
  $d1['month'] = $d[1];
  $d1['day'] = $d[2];
  return $d1;
}
/**
 * Utility method for converting from a date array to
 * a Unix timestamp.  Accepts 'Type', an optional argument to
 * specify the end of the day or the beginning of the day
 * @param date array $array
 * @param string $type
 * @return number|number
 */
function _up_multi_arraytostamp($array, $type='start') {
  if ($type=='start') {
    return gmmktime(0, 0, 0, $array['month'], $array['day'], $array['year']);
  }
  else {
    return gmmktime(23, 59, 59, $array['month'], $array['day'], $array['year']);
  }
}

function up_multi_stock_report() {
  $headers = array(
  array('data' => 'Product'),

  );
  $rows = array();
  foreach (up_multi_stores() as $store) {
    $headers[] = array('data' => $store->store_name);
    $stores[] = $store->store_id;
  }
  $headers[] = array('data' => 'Total');
  $query = "SELECT p.model, p.nid, n.title AS title FROM {uc_products} p LEFT JOIN {uc_product_stock} ON sku = model, node n WHERE p.nid = n.nid";
  $universal_stock = pager_query($query, 20);

  while ($value = db_fetch_array($universal_stock)) {
    $shown_models[] = $value['model'];
    $rows[$value['model']] = array(
    'title' => l($value['title'], 'node/'. $value['nid'] .'/edit/stock'),
    );
    foreach ($stores as $store) {
      $rows[$value['model']][$store] = 0;
    }
    $rows[$value['model']]['total'] = 0;
  }
  $per_store_stock = db_query("SELECT p.model, store_id AS sid, stock_level as stock FROM {uc_products} p, {up_store_stock} sp WHERE p.model = sp.model AND p.model IN (". db_placeholders($shown_models) .")", $shown_models);

  while ($value = db_fetch_array($per_store_stock)) {
    $rows[$value['model']][$value['sid']] = $value['stock'];
    $rows[$value['model']]['total'] += $value['stock'];
  }
  return theme('table', $headers, $rows) . theme('pager');
}

function up_multi_report_filter_submit($form, &$form_state) {
  $start_date = _up_multi_arraytostamp($form_state['values']['start_date'], 'start');
  $end_date = _up_multi_arraytostamp($form_state['values']['end_date'], 'end');

  $args = array(
    'start_date' => $start_date,
    'end_date' => $end_date
  );

  if ($form_state['values']['location']) {
    array_push($args, $form_state['values']['location']);
  }
  $form_state['redirect'] = $form_state['values']['redirect'] .'/'. implode('/', $args);
}

/**
 * Helper function for PHP < 5.3
 * Needed for admin report array manipulation
 */
if (!function_exists('array_replace')) {
  function array_replace( array $array, array $array1 ) {
    $args = func_get_args();
    $count = func_num_args();

    for ($i = 0; $i < $count; ++$i) {
      if (is_array($args[$i])) {
        foreach ($args[$i] as $key => $val) {
          $array[$key] = $val;
        }
      }
      else {
        trigger_error(
          __FUNCTION__ . '(): Argument #' . ($i+1) . ' is not an array',
          E_USER_WARNING
        );
        return NULL;
      }
    }

    return $array;
  }
}