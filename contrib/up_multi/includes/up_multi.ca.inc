<?php

/**
 * @file
 * Contains conditional action functions for uberpos multistore.
 */

/**
 * Implementation of hook_action().
 */
function up_multi_ca_action() {
  $actions['up_multi_action_decrement_stock'] = array(
    '#title' => t('Decrement stock of a product from a specific store.'),
    '#callback' => 'up_multi_action_decrement_stock',
    '#arguments' => array(
      'order' => array('#entity' => 'uc_order', '#title' => t('Order')),
      'product' => array('#entity' => 'uc_order_product', '#title' => t('Product')),
    ),
    '#category' => t('Stock'),
  );

  $actions['up_multi_action_increment_stock'] = array(
    '#title' => t('Increment stock of a product from a specific store.'),
    '#callback' => 'up_multi_action_increment_stock',
    '#arguments' => array(
      'order' => array('#entity' => 'uc_order', '#title' => t('Order')),
      'product' => array('#entity' => 'uc_order_product', '#title' => t('Product')),
    ),
    '#category' => t('Stock'),
  );
  return $actions;
}

function up_multi_action_decrement_stock($order, $product, $settings) {
  $store = $order->store_id;
  global $user;
  $stock_warnings = array();
  
  if (($stock = up_multi_stock($product->model, $store)) !== FALSE) {
    $threshold = db_result(db_query("SELECT threshold FROM {uc_product_stock} WHERE sku = '%s'", $product->model));
    if ((($stock - $product->qty) <= $threshold) && !in_array($product->model, array_keys($stock_warnings))) {
      $stock_warnings[$product->model] = $stock;
    }

    $stock -= $product->qty;
    up_multi_stock_adjust($product->model, -$product->qty, $store);
    uc_order_comment_save($order->order_id, $user->uid, t('The stock level for %model_name at location %store has been decreased to !qty.', array('%model_name' => $product->title, '%store' => $store, '!qty' => ($stock - $product->qty))));

  }

  if (!empty($stock_warnings) && variable_get('uc_stock_threshold_notification', FALSE)) {
    foreach ($stock_warnings as $model => $stock) {
      _uc_stock_send_mail($order, $stock);
    }
  }
}

function up_multi_action_increment_stock($order, $product, $settings) {
  $store = $order->store_id;
  if (($stock = up_multi_stock($product->model, $store)) !== FALSE) {
    up_multi_stock_adjust($product->model, $product->qty, $store);
    uc_order_comment_save($order->order_id, $user->uid, t('The stock level for %model_name at location %store has been increased to !qty.', array('%model_name' => $product->title, '%store' => $store, '!qty' => ($stock - $product->qty))));
  }
}
