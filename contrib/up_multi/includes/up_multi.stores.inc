<?php

/*
 * @file
 * Contains forms and functions for multistore CRUD operations
 * All functions should directly relate to adding/editing stores
 * or users.
 */
function up_multi_store_form($form_state, $form_id=NULL, $op=NULL, $sid=NULL) {
    $store = array();
    if ($sid) {
        $store = db_fetch_array(db_query('SELECT * FROM {up_stores} WHERE store_id = %d', arg(5)));
    }
    if ($store) {
            $form['store_id'] = array(
                    '#type' => 'value',
                    '#value' => $sid,
            );
    }
    $form['store_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Store Name'),
            '#default_value' => $store['store_name'],
            '#size' => 20,
            '#maxlength' => 100,
            '#required' => TRUE,
    );
    $form['store_address'] = array(
            '#type' => 'textfield',
            '#title' => t('Address'),
            '#maxlength' => 100,
            '#default_value' => $store['store_address'],
    );
    $form['store_city'] = array(
            '#type' => 'textfield',
            '#title' => t('City'),
            '#maxlength' => 20,
            '#size' => 20,
            '#default_value' => $store['store_city'],
            '#attributes' => array('autocomplete' => 'off'),
    );
    $form['store_state'] = array(
            '#type' => 'textfield',
            '#title' => t('State/Province'),
            '#maxlength' => 20,
            '#size' => 20,
            '#default_value' => $store['store_state'],
    );
    $form['store_zip'] = array(
            '#type' => 'textfield',
            '#title' => t('Postal Code'),
            '#maxlength' => 12,
            '#size' => 10,
            '#default_value' => $store['store_zip'],
    );
    $form['store_country'] = array(
            '#type' => 'textfield',
            '#title' => t('Country'),
            '#size' => 20,
            '#default_value' => $store['store_country'],
    );
    $form['store_phone'] = array(
            '#type' => 'textfield',
            '#title' => t('Store Phone'),
            '#maxlength' => 20,
            '#size' => 20,
            '#default_value' => $store['store_phone'],
    );
    $form['store_fax'] = array(
            '#type' => 'textfield',
            '#title' => t('Store Fax'),
            '#maxlength' => 20,
            '#size' => 23,
            '#default_value' => $store['store_fax'],
    );
    $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('submit'),
    );
    if ($op = "add") {
      $form['#redirect'] = 'admin/store/UP/stores';
    }

    return $form;
}


function up_multi_store_list() {
    $result = db_query("SELECT store_name, store_id FROM {up_stores}");
    $rows = array();

    $header[] = array('data' => t('Store Name'));
    $header[] = array('data' => t('Edit Link'));
    $header[] = array('data' => t('Delete Link'));

    while ($data = db_fetch_object($result)) {
      $rows[$data->store_id][] = $data->store_name;
      $rows[$data->store_id][] = l(t('Edit'), 'admin/store/UP/stores/edit/'. $data->store_id);
      $rows[$data->store_id][] = l(t('Delete'), 'admin/store/UP/stores/remove/'. $data->store_id);
    }
    if (!$rows) {
      return t('You have not added any stores yet.  To add a store, go to') .' '. l('Add a Store', 'admin/store/UP/stores/add');
    }
    return theme('table', $header, $rows);
}

function up_multi_store_form_submit($form, &$form_state) {
    if ($form_state['values']['store_id']) {
        drupal_write_record('up_stores', $form_state['values'], 'store_id');
    }
    else {
        drupal_write_record('up_stores', $form_state['values']);
    }
}

function up_multi_store_delete_form($form_state, $form_id, $sid) {
    if ($sid) {
        $result = db_result(db_query('SELECT store_name FROM {up_stores} WHERE store_id = %d', $sid));
    }
    if (!$result) {
        drupal_set_message(t("No store selected for deletion."), 'error');
        watchdog('up_multi', 'Store deletion page was called without an argument or with an invalid argument.', '', WATCHDOG_WARNING);
        drupal_goto('admin/store/UP/stores');
    }
    $message = t('WARNING: Deleting a store can make it impossible to retrieve past records for that store.  Are you sure you want to delete %store?', array('%store' => $result));
    drupal_set_message($message, 'error');
    $form['store_id'] = array(
            '#type' => 'value',
            '#value' => $sid,
    );
    $form['confirm'] = array(
            '#type' => 'checkbox',
            '#title' => t('I am sure'),
    );
    $form['store_name'] = array(
            '#type' => 'value',
            '#value' => $result,
    );

    $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
    );
    return $form;
}

function up_multi_store_delete_form_submit($form, &$form_state) {
    if ($form_state['values']['confirm'] && $form_state['values']['store_id']) {
        db_query('DELETE FROM {up_stores} WHERE store_id=%d', $form_state['values']['store_id']);
        if (db_affected_rows()==1) {
            $mess = 'Successfully deleted %store from UberPOS locations';
            $var = array('%store' => $form_state['values']['store_name']);
            drupal_set_message(t($mess, $var));
            watchdog('up_multi', $mess, $var);
            drupal_goto('admin/store/UP/stores');
        }
        else {
            $mess = 'Unable to delete %store from database.';
            $var = array('%store' => $form_state['values']['store_name']);
            drupal_set_message(t($mess, $var));
            watchdog('up_multi', $mess, $var);
        }
    }
    else {
        drupal_set_message(t("If you really want to delete this store, you must click 'I am sure.'"), 'error');
    }
}
/*
 * Form displaying user/store permissions
 * Note: This is a very expensive function, but since it shouldn't be run often,
 * it should work for the time being.
 */
function up_multi_user_permissions(&$form_state, $user = NULL) {
    if (!$user) {
        $all_users = array();
        $result = db_query('SELECT uid, name FROM {users} WHERE status=1');
        while ($us = db_fetch_object($result)) {
            $all_users[] = $us;
        }
    }
    else {
        $all_users[] = $user;
    }

    $all_auth = db_result(db_query('SELECT rid FROM {permission} WHERE rid=2 AND perm LIKE "%use pos system%"'));
    foreach ($all_users as $us) {
        if ($us->uid < 2) {
            continue;
        } //Exclude root
        if ($all_auth) {
            $users[] = $us;
        }
        else {
            if (!$us->roles) {
                $role_check = db_query("SELECT rid FROM {users_roles} WHERE uid=%d", $us->uid);
                while ($role = db_result($role_check)) {
                    $us->roles[$role] = '';
                }
            }
            if ($us->roles) {
                if (user_access('use pos system', $us)) {
                    $users[] = $us;
                }
            }
        }
    }
    $stores = up_multi_stores();
    if (empty($stores)) {
      drupal_set_message(t('No stores have been added.  Please add a store.'), 'warning');
    }
    $options = array();
    foreach ($stores as $store) {
        $options[$store->store_id] = '';
        $form['table']['stores'][$store->store_id] = array(
                '#type' => 'item',
                '#value' => $store->store_name,
        );
    }
    $form['table']['users'] = array(
            '#type' => 'fieldset',
            '#title' => 'Users',
            '#tree' => TRUE,
    );
    if (!$users) {
        drupal_set_message(t("No users have been given POS permission.  Please check permissions"), 'warning');
        drupal_goto('admin/user/permissions');
    }
    foreach ($users as $user) {
        $result = db_result(db_query('SELECT stores FROM {up_store_users} WHERE uid=%d', $user->uid));
        $selected = ($result) ? unserialize($result): array();

        $form['table']['users'][$user->uid] = array(
                '#type' => 'fieldset',
                '#value' => $user->name,
                '#title' => $user->name,
        );
        $form['table']['users'][$user->uid]['stores'] = array(
                '#type' => 'checkboxes',
                '#options' => $options,
                '#default_value' => $selected,
                '#update' => ($result) ? 1 : 0,
        );

    }
    $form['submit'] = array(
            '#type' => 'submit',
            '#value' => 'Save Settings',
    );
    $form['table']['#theme'] = 'store_user_permtable';

    return $form;
}
/*
 * Hook_theme for displaying user/store permissions table.
 */
function theme_store_user_permtable($form) {
    $header = array();
    $rows = array();

    foreach ($form['users'] as $user => $data) {
        $row = NULL;
        if (is_numeric($user) && is_array($data)) {
            $name = '<div class="username">'. $data['#value'] .'</div>';
            $row[] = $name;
            foreach ($data['stores'] as $store => $d) {
                if (is_numeric($store) && is_array($d)) {
                    $row[] = array('data' => drupal_render($d));
                }
            }
            $rows[] = $row;
        }
    }
    $header[] = array('data' => t("User Name"));
    if (is_array($form['stores'])) {
      foreach ($form['stores'] as $store => $data) {
          if (is_numeric($store) && is_array($data)) {
              $header[] = array('data' => drupal_render($data), 'class' => 'checkbox');
          }
      }

    }

    $output = theme('table', $header, $rows, array('id' => 'up_permissions'));
    $output .= drupal_render($form['submit']);
    return $output;
}

function up_multi_user_permissions_submit($form, &$form_state) {
    foreach ($form_state['values']['users'] as $user => $values) {
        $update = $form['table']['users'][$user]['stores']['#update'];
        $values['stores'] = array_filter($values['stores'], 'up_array_check');
        $record->uid = $user;
        $record->stores = serialize($values['stores']);
        if ($update) {
            drupal_write_record('up_store_users', $record, 'uid');
        }
        else {
            drupal_write_record('up_store_users', $record);
        }
    }
    drupal_set_message(t("User Store Permissions Saved."));

}
//TODO: Find a cleaner way to do this
function up_array_check($x) {
    return $x;
}
