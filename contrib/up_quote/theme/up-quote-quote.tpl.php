<?php

/* 
 * @file
 * Default template for uberpos quotes
 * 
 * Available vars:
 * $order - stdclass object containing all the uc_order attributes
 * $customer - stdclass object containing the customer's user data
 * $user - stdclass object containing the cashier's user data
 *
 */

?>
<div id="quote_header"><?php print $header; ?></div>
<br />
<h1>Price Quote</h1>
<div id="meta">
<b>Prepared for:</b><span><?php print $customer->name; ?> (#<?php print $customer->uid;?>)</span><br />
<b>Prepared by:</b><span><?php print $user->name; ?></span><br />
<b>Quote #:</b><span><?php print $order->order_id; ?></span><br />
<b>Date:</b><span><?php print $created; ?></span><br />
</div>
<br />
<table id="quote_contents" width ="100%">
    <tr>
        <th width="50%">Item</th>
        <th width="25%">Quantity</th>
        <th width="25%">Price</th>
    </tr>
    <?php foreach ($order->products as $product) : ?>
    <tr>
    <td width="50%"><span class="item_name" style="font-weight: bold;"><?php print $product->title; ?></span></td>
    <td width="25%"><span class="item_qty"><?php print $product->qty; ?></span></td>
    <td width="25%"><span class="item_price"><?php print $product->price ?></span></td>
    </tr>
    <?php endforeach ?>
    <tr></tr>
    <?php foreach($order->line_items as $item) : ?>
    <tr>

    <td width="25%"><span class="item_name" style="font-weight: bold;"><?php print $item['title']; ?></span><td>
    <td width="25%"><span class="item_price"><?php print $item['amount']; ?></span></td>
    </tr>
    <?php endforeach ?>
</table>
<?php if($barcode) : ?>
<div id="quote_barcode"><?php print $barcode; ?></div>
<?php endif ?>

<div id="quote_footer"><?php print $footer; ?></div>
