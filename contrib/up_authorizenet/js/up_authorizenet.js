/**
 * Behaviors for UberPOS Authorize.net.
 */
Drupal.behaviors.up_authorizenet = function() {
  $('input[name="up_authorizenet_uc_authorizenet"]').click(function(){
    up_authorizenet_toggle_fieldset($(this).val());
  });
  
  // Initialize.
  up_authorizenet_toggle_fieldset($('input[name="up_authorizenet_uc_authorizenet"]:checked').val());
}

function up_authorizenet_toggle_fieldset(shown) {
  if (shown == 0) {
    $('#up-authorizenet-fieldset').show();
  }
  else {
    $('#up-authorizenet-fieldset').hide();
  }
}