This module overrides the default UberPOS CC command to allow you to swipe a 
credit card or enter in the number and expiration date manually.

To set up:
1) Install and enable the module at admin/build/modules.
2) Enable it for use at /admin/store/settings/pos and enter your login 
information for authorize.net. IMPORTANT NOTE: This login information *MUST*
correspond to an account which allows Card Present Transactions.
3) Set up is done. When the CC button is pressed you should now be able to 
swipe the card with a USB credit card reader, or you can enter in the 
cardnumber and expiration date together and press enter.

For details of the authorize.net Card Present API, please visit:
http://developer.authorize.net/api/


