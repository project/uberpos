Drupal.Uberpos = {};
Drupal.Uberpos.top = 0;
Drupal.Uberpos.rowsPerScreen = 0;
Drupal.Uberpos.firstPass = true;

/*****************************************************************************
 * Interface setup
 *****************************************************************************/

/*
 * All behaviors go in here.
 */
Drupal.behaviors.mainScreen = function(context) {
  $('.up-text-submit:not(.up-text-submit-processed)', context).addClass('up-text-submit-processed').each(function () {
    $(this, context).click(function() {
      uberpos_add_text_submit($(this).children('.up-text').text());
      return false;
    });
  });

  $('.up-add-text-no-submit:not(.up-add-text-no-submit-processed)', context).addClass('up-add-text-no-submit-processed').each(function () {
    $(this).click(function() {
      uberpos_add_text($(this).children('.up-text').text());
      return false;
    });
  });

  $('.up-button:not(.up-button-processed)', context).addClass('up-button-processed').each(function () {
    $(this).click(function() {
      uberpos_add_text($(this).text());
      return false;
    });
  });

  $('#uberpos-interface #screen table th:not(.uberpos-screen-processed)', context).addClass('uberpos-screen-processed').each(function () {
     $(this).hide();
  });

  $('#uberpos-interface input#edit-pos-input:not(.edit-pos-input-processed)', context).addClass('edit-pos-input-processed').each(function () {
    $(this).focus();
  });
};

/*
 * Stuff I haven't gotten into proper behaviors yet. heh.
 *
 * Run uberpos_screen_ajax() to get the welcome message, override the key up,
 * down and enter events, and start the clock.
 */
Drupal.behaviors.uberposScreen = function() {
  $(window).load(function() {
    /* These should never be more then one. Probably a better way to do this */
    $('#uberpos-interface #screen').each(function() {
      uberpos_screen_ajax();
    });
    holdFocus = false;

    $(document).keydown(function(event) {
      switch (event.which) {
        case 9: /*TAB*/
          if(event.shiftKey) {
            holdFocus = Drupal.Uberpos.shiftFocus(holdFocus);
            return false;
          }
          return true;
        case 13: /* ENTER */
          uberpos_screen_ajax();
          return false;
        case 40:
          Drupal.Uberpos.keyDown();
          return false;
        case 38:
          Drupal.Uberpos.keyUp();
          return false;
        default:
          if(holdFocus && !$(event.target).is('.form-textarea, .form-text')) {
            $('#edit-pos-input').focus();
            //IE puts cursor at the beginning of the text box unless we do this.  
            $('#edit-pos-input').val($('#edit-pos-input').val());
            
          }
          return true;
      }
    });

    Drupal.Uberpos.updateTime(this); 
  });
}


Drupal.Uberpos.shiftFocus = function(holdFocus) {
  $('#edit-pos-input').toggleClass('focused');
  return !holdFocus;
}

/*
 * This is just so people with programmable keyboards can switch from the
 * username and password textfields on the login screen with there up and down
 * arrows.
 */
Drupal.behaviors.uberposLogin = function(context) {
  $('input#edit-name:not(.up-login-processed)', context).addClass('up-login-processed').each(function () {
    $(this).focus();

    $(window).keydown(function(event) {
      switch (event.keyCode) {
        case 40:
          $("input#edit-pass").focus();
          break;
        case 38:
          $("input#edit-name").focus();
      }
    });
  });
}

/*
 * Updates the clock every second.
 * TODO, this isn't something that is easily themed, but ideally it would be.
 */
Drupal.Uberpos.updateTime = function() {
  var currentTime = new Date()
  var month = currentTime.getMonth() + 1
  var day = currentTime.getDate()
  var year = currentTime.getFullYear()
  var today = new Date();
  var hour = today.getHours();
  var mins = today.getMinutes();
  var secs = today.getSeconds();

  if (secs <= 9){
      secs = "0" + secs;
  }

  if (mins <= 9){
      mins = "0" + mins;
  }


  if(hour > 11){
    var am_or_pm = "PM";
  } else {
    var am_or_pm = "AM";
  }

  var datetime = month + "/" + day + "/" + year + " " + hour + ":" + mins + ":" + secs + " " + am_or_pm;

  $('#uberpos-date').text(datetime); 
  setTimeout("Drupal.Uberpos.updateTime()", 1000) 
}

/*
 * jPrintArea jQuery Plugin:
 * http://plugins.jquery.com/project/jPrintArea
 *
 * Used for printing the receipts.
 */
Drupal.Uberpos.jPrintArea=function(el)
{
  var iframe=document.createElement('IFRAME');
  var doc=null;
  
  $(iframe).attr('style','position:absolute;width:0px;height:0px;left:-500px;top:-500px;');

  document.body.appendChild(iframe);
  doc=iframe.contentWindow.document;
  var links=window.document.getElementsByTagName('link');

  for(var i=0;i<links.length;i++)
    if(links[i].rel.toLowerCase()=='stylesheet')
      doc.write('<link type="text/css" rel="stylesheet" href="'+links[i].href+'"></link>');

  doc.write('<div class="'+$(el).attr("class")+'">'+$(el).html()+'</div>');
  doc.close();

  $("img").load(function() {
    iframe.contentWindow.focus();
    iframe.contentWindow.print();
  });
}

/*****************************************************************************
 * Special input functions
 *****************************************************************************/

/*
 * Scroll the item list up once.
 */
Drupal.Uberpos.keyUp = function() {
  prev = parseInt($('table#main-table tr.selected').attr('id'));

  if(prev > 0) {
    select = prev - 1;
    $("#screen table tbody tr").eq(prev).removeClass("selected");
    $("#screen table tbody tr").eq(select).addClass("selected");

    if(select < Drupal.Uberpos.top) {
      Drupal.Uberpos.top = select;
      $("div#screen").scrollTo($("#screen table tbody tr").eq(select));
    }
  }

  $("input#edit-pos-input").focus();
}

/*
 * Scroll the item list down once.
 */
Drupal.Uberpos.keyDown = function() {
  Drupal.Uberpos.down(parseInt($('table#main-table tr.selected').attr('id')));
  $("input#edit-pos-input").focus();
}

/*
 * Scroll the item list down based on 'prev'.
 */
Drupal.Uberpos.down = function(prev) {
  select = prev + 1;

  if($("#screen table#main-table tbody tr").eq(select).length > 0) {
    $("#screen table#main-table tbody tr").eq(prev).removeClass("selected");      
    $("#screen table#main-table tbody tr").eq(select).addClass("selected");
 
    offset = (-1 * $("#screen").height()) + $("#screen table#main-table tbody tr").eq(select).height();

    if(select >= Drupal.Uberpos.top + Drupal.Uberpos.rowsPerScreen) {
      Drupal.Uberpos.top = select - Drupal.Uberpos.rowsPerScreen + 1;
      $("div#screen").scrollTo($("#screen table#main-table tbody tr").eq(Drupal.Uberpos.top));
    }
  }
}

/*
 * Add text to the input field but don't submit it.
 */
function uberpos_add_text(text) {
  $('input#edit-pos-input').val($('input#edit-pos-input').val() + text);
  $("input#edit-pos-input").focus();
}

/*
 * Add text to the uberpos input field and submit it.
 */
function uberpos_add_text_submit(text) {
  uberpos_add_text(text);
  uberpos_screen_ajax();
}

/*****************************************************************************
 * Ajax fun below.
 *****************************************************************************/

/*
 * Posts the AJAX request.
 */
function uberpos_ajax_execute(input) {
  var sel = $('table#main-table tr.selected td.product-title').attr('id');

  show_uberpos_throbber();

  $.ajax({
    type: "POST",
    url: Drupal.settings.basePath + '?q=admin/store/pos/ajax',
    data: 'order_id=' + Drupal.settings.uberpos.order_id + '&input=' + escape(input) + '&item=' + sel,
    success: function(data) {
      uberpos_screen_ajax_success(data);
      hide_uberpos_throbber();
    }

  });

  return false;
}

/*
 * Get the input for the text box, clear the text box, run the AJAX command.
 */
function uberpos_screen_ajax() {
  input = $('input#edit-pos-input').val();
  $('input#edit-pos-input').val('');

  uberpos_ajax_execute(input);

  return false;
}

/*
 * AJAX executed successfully, time to process. This is the meat of the AJAX
 * code.
 */
function uberpos_screen_ajax_success(data) {
  var prev = parseInt($('table#main-table tr.selected').attr('id'));

  /* Flush old data */
  $("#screen table#main-table tbody tr").remove();
  $(".uberpos-line-item").remove();
  $('#screen #welcome-message').empty();
  $('#screen #clear-message').remove();
  $("#receipt").empty();

  /* New data for updating the screen. 
   * Includes products, line items, customer name. */
  var result = Drupal.parseJson(data);

  Drupal.settings.uberpos.order_id = result['order_id'];

  if(result['redirect']) {
    window.location = result['redirect'];
  }
  else if(result['clear_message']) {
    $('#screen #clear-message-container').html('<div id="clear-message">' + result['clear_message'] + '</div>');
  } else if(result['welcome']) {
   $('#screen #welcome-message').html(result['welcome']);
  } else {
    /* Add new products */
    for(var i in result['products']) {
      p = result['products'][i];
      $('#screen table#main-table tbody').append('<tr id=' + i + ' class="' + p['class'] + '"><td id="' + p['type'] + p['id'] + '" class="product-title"><div class="product-title">'+ p['title'] +'</td><td class="price">' + p['price'] + '</div></td></tr>');
    }

    /* Update scrolling interface */ 
    Drupal.Uberpos.rowsPerScreen = Math.floor($("#screen").height() / $("#screen table tbody tr").eq(0).height()) - 1;
  
    if(Drupal.Uberpos.firstPass == true || result['action'] == 'new' ||
       result['action'] == 'loaded') {
      Drupal.Uberpos.down(parseInt($('table#main-table tr:last').attr('id') ) - 1);
      Drupal.Uberpos.firstPass = false;
    } else {
      if($("#screen table#main-table tbody tr").eq(prev).length > 0) {
        $("#screen table#main-table tbody tr").eq(prev).addClass("selected");
      } else if($("#screen table#main-table  tbody tr").eq(prev-1) > 0) { 
        $("#screen table#main-table  tbody tr").eq(prev-1).addClass("selected");
      } else {
        $("#screen table#main-table tbody tr:last").addClass("selected");
      }
    }
  }

  /* Add any existing line items, total, due etc */
  for(var i in result['line_items']) {
    l = result['line_items'][i];
    $('#line-items').append('<div class="uberpos-line-item">' + l['title'] + ": " + l['amount'] + '</div>');
  }

  /* Update customer in case the ID command was used */
  $("span.customer").text(result['customer']);

  if(result['receipt']) {
    $("#receipt").html(result['receipt']);
    $("#receipt .receipt").each(function() {
      Drupal.Uberpos.jPrintArea(this);
    });

    $("input#edit-pos-input").focus();
  }
}

/**
 * User feedback that something is happening.
 */
function show_uberpos_throbber() {
  $('#uberpos-div-throbber').attr('style', 'background-image: url(' + Drupal.settings.basePath + 'misc/throbber.gif); display: inline-block; background-repeat: no-repeat; background-position: 100% -20px;').html('&nbsp;&nbsp;&nbsp;&nbsp;');
}

/**
 * Done loading forms.
 */
function hide_uberpos_throbber() {
  $('#uberpos-div-throbber').removeAttr('style').empty();
}


