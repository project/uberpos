
$(document).ready(stockAdjustWidget);

function stockAdjustWidget() {

    var container = $('<div></div>');
    var amount = $('<input type="text" name="amount" size="2" />');
    var target = $('<select name="target"></select>');
    var source = $('<select name="source"></select>');

    var stores = $('.store-stock').each(function() {
        target.append(
            $('<option value=' +
                $(this).attr('id') + '>' +
                $(this).parents('tr').children('td.store_name').text()+
                '</option>')
        );
        source.append(
            $('<option value=' +
                $(this).attr('id') + '>' +
                $(this).parents('tr').text()+
                '</option>')
        );
    });

    var calcul = $('<input type="button" value="Adjust" />');
    calcul.click(function() {
        sourceEl = $('#'+source.val());
        targetEl = $('#'+target.val());
        if(amount.val() > sourceEl.val()) {
          alert('You have entered an invalid adjustment.  Please try again.');
        }
        else {
          thisAmount = parseInt(sourceEl.val()) - parseInt(amount.val());
          thatAmount = parseInt(targetEl.val()) + parseInt(amount.val());
          
          sourceEl.attr('value', thisAmount);
          targetEl.attr('value', thatAmount);
        }        
    })
    container.append('Move ');
    container.append(amount);
    container.append(' units from ');
    container.append(source);
    container.append(' to ');
    container.append(target);
    container.append(calcul);
    container.insertBefore($(stores[0]).parents('table'));
}

