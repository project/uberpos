<?php

/**
 * @file
 * Adds a custom views field for making buttons.
 */

/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function uberpos_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'uberpos') . '/views',
    ),
    'handlers' => array(
      // field handlers
      'uberpos_handler_field_custom' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data().
 */
function uberpos_views_data() {
  $data['uberpos']['table']['group'] = t('UberPOS');
  $data['uberpos']['table']['join'] = array(
    '#global' => array(),
  );

  $data['uberpos']['pos'] = array(
    'title' => t('Custom UberPOS product field link'),
    'help' => t('This field can be used to create product links that, when clicked, will cause the product to be added to the current order.'),
    'field' => array(
      'handler' => 'uberpos_handler_field_custom',
    ),
  );

  return $data;
}


