<?php

/**
 * @file
 * Handler for custom UberPOS view field.
 */

/**
 * A handler to provide a field that is completely custom by the administrator.
 */
class uberpos_handler_field_custom extends views_handler_field {
  function query() {
    // do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['alter']['contains']['alter_text'] = array('default' => TRUE);
    $options['alter']['contains']['should_not_submit'] = array('default' => FALSE);
    $options['alter']['input'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['alter']['input'] = array(
      '#title' => t('Input for POS system'),
      '#type' => 'textfield',
      '#default_value' => $this->options['alter']['input'],
      '#description' => t('This text will be entered into the POS as if typed into the keyboard. You can may use available tokens (see "Replacement patterns" below).'),
      '#weight' => -1,
    );

    $form['alter']['should_not_submit'] = array(
      '#title' => t('This command should not be submitted'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['alter']['should_not_submit'],
      '#description' => t('The default is for the command to be entered and submitted'),
    );

    $form['alter']['text']['#title'] = t('Output');
    $form['alter']['text']['#description'] = t('This may contain HTML and images from below. The output will be shown to the cashier as a link. When click it will submit the command specified in the "input" field above. You may use available tokens (see "Replacement patterns" below).');
    // Make text field non-dependent on checkbox
    unset($form['alter']['text']['#dependency'], $form['alter']['text']['#process']);

    // Get rid of the default stuff we don't need
    unset($form['alter']['make_link']);
    unset($form['alter']['path']);
    unset($form['alter']['link_class']);
    unset($form['alter']['alt']);
    unset($form['alter']['prefix']);
    unset($form['alter']['suffix']);
    unset($form['alter']['target']);
    unset($form['alter']['alter_text']);
    unset($form['alter']['trim']);
    unset($form['alter']['max_length']);
    unset($form['alter']['word_boundary']);
    unset($form['alter']['ellipsis']);
    unset($form['alter']['strip_tags']);
    unset($form['alter']['html']);
  }

  function render_altered($alter, $tokens) {
    /* Prepare the command */
    $input = check_plain($alter['input']);
    $input = strtr($input, $tokens);

    /* Prepare output and put command in the place where the JS will look for
     * it. */
    $text = filter_xss_admin($alter['text']);
    $text = strtr($text, $tokens);
    $text .= '<div class="up-text" style="display:none">' . $input . '</div>';

    if ($alter['should_not_submit']) {
      $class = 'up-add-text-no-submit';
    }
    else {
      $class = 'up-text-submit';
    }

    return l($text, '', array('attributes' => array('class' => $class), 'html' => TRUE));
  }
}
