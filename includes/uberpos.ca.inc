<?php

/**
 * @file
 * This file contains the Conditional Actions hooks and functions necessary to
 * make the uberpos stock related entity, conditions, events, and actions work.
 */

/******************************************************************************
 * Conditional Actions Hooks                                                  *
 ******************************************************************************/

/**
 * Implementation of hook_ca_entity().
 */
function uberpos_ca_entity() {
  $entities['uc_order_product'] = array(
    '#title' => t('UC Order Product object'),
    '#type' => 'object',
  );
  return $entities;
}

/**
 * Implementation of hook_ca_trigger().
 */
function uberpos_ca_trigger() {
  $triggers['uberpos_item_added'] = array(
    '#title' => t('Product added to uberpos order'),
    '#category' => t('UberPOS'),
    '#arguments' => array(
      'order' => array(
        '#entity' => 'uc_order',
        '#title' => t('Order'),
      ),
      'product' => array(
        '#entity' => 'uc_order_product',
        '#title' => t('Product that was added'),
      )
    ),
  );

  $triggers['uberpos_item_removed'] = array(
    '#title' => t('Product removed from uberpos order (does not apply to refunds)'),
    '#category' => t('UberPOS'),
    '#arguments' => array(
      'order' => array(
        '#entity' => 'uc_order',
        '#title' => t('Order'),
      ),
      'product' => array(
        '#entity' => 'uc_order_product',
        '#title' => t('Product that was removed'),
      )
    ),
  );
  
  $triggers['uberpos_item_refunded'] = array(
    '#title' => t('Product refunded from uberpos order'),
    '#category' => t('UberPOS'),
    '#arguments' => array(
      'order' => array(
        '#entity' => 'uc_order',
        '#title' => t('Order'),
      ),
      'product' => array(
        '#entity' => 'uc_order_product',
        '#title' => t('Product that was refunded'),
      )
    ),
  );

  return $triggers;
}

/**
 * Implementation of hook_action().
 */
function uberpos_ca_action() {
  $actions['uberpos_action_decrement_stock'] = array(
    '#title' => t('Decrement stock of a product with tracking activated.'),
    '#callback' => 'uberpos_action_decrement_stock',
    '#arguments' => array(
      'order' => array('#entity' => 'uc_order', '#title' => t('Order')),
      'product' => array('#entity' => 'uc_order_product', '#title' => t('Product')),
    ),
    '#category' => t('Stock'),
  );

  $actions['uberpos_action_increment_stock'] = array(
    '#title' => t('Increment stock of a product with tracking activated.'),
    '#callback' => 'uberpos_action_increment_stock',
    '#arguments' => array(
      'order' => array('#entity' => 'uc_order', '#title' => t('Order')),
      'product' => array('#entity' => 'uc_order_product', '#title' => t('Product')),
    ),
    '#category' => t('Stock'),
  );

  return $actions;
}


/******************************************************************************
 * Conditional Action Callbacks and Forms                                     *
 ******************************************************************************/

/**
 * Decrease the stock of a product.
 */
function uberpos_action_decrement_stock($order, $product, $settings) {
  $stock_warnings = array();
  
  if (($stock = uc_stock_level($product->model)) !== FALSE) {
    $stock_level = db_fetch_object(db_query("SELECT * FROM {uc_product_stock} WHERE sku = '%s'", $product->model));
    if ((($stock - $product->qty) <= $stock_level->threshold) && !in_array($product->model, array_keys($stock_warnings))) {
      $stock_level->stock -= $product->qty;
      $stock_warnings[$product->model] = $stock_level;
    }
    uc_stock_adjust($product->model, -$product->qty);
    uc_order_comment_save($order->order_id, 0, t('The stock level for %model_name has been decreased to !qty.', array('%model_name' => $product->model, '!qty' => ($stock - $product->qty))));
  }

  if (!empty($stock_warnings) && variable_get('uc_stock_threshold_notification', FALSE)) {
    foreach ($stock_warnings as $model => $stock_level) {
      _uc_stock_send_mail($order, $stock_level);
    }
  }
}

/**
 * Increment the stock of a product.
 */
function uberpos_action_increment_stock($order, $product, $settings) {
  if (($stock = uc_stock_level($product->model)) !== FALSE) {
    uc_stock_adjust($product->model, $product->qty);
    uc_order_comment_save($order->order_id, 0, t('The stock level for %model_name has been increased to !qty.', array('%model_name' => $product->model, '!qty' => ($stock - $product->qty))));
  }
}
