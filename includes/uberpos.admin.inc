<?php

/**
 * @file
 * UberPOS administration forms.
 */

function uberpos_settings_form() {
  $form['tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available Tokens'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    'tokens' => array(
        '#value' => theme('token_help', 'global'),
    )
  );
  $form['show_help'] = array(
    '#type' => 'radios',
    '#title' => t('Training: show help links on UberPOS buttons'),
    '#default_value' => variable_get('uberpos_show_help', 0),
    '#options' => array(t('No'), t('Yes')),
    '#description' => t('Requires the advanced help module.'),
    '#weight' => -7,
    '#disabled' => module_exists('advanced_help') ? FALSE : TRUE,
  );

  $form['welcome_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Welcome text'),
    '#default_value' => variable_get('uberpos_welcome_body', ''),
    '#rows' => 10,
    '#weight' => -6,
  );

  $form['welcome_format'] = filter_form($welcome_format = variable_get('uberpos_welcome_format', FILTER_FORMAT_DEFAULT), -5, array('welcome_format'));

  $form['receipt_header_field'] = array(
    '#type' => 'textarea',
    '#title' => t('Receipt header'),
    '#default_value' => variable_get('uberpos_receipt_header_body', ''),
    '#rows' => 10,
    '#weight' => -4,
  );

  $form['receipt_header_field_format'] = filter_form(variable_get('uberpos_receipt_header_format', FILTER_FORMAT_DEFAULT), -3, array('receipt_header_field_format'));

  $form['receipt_footer_field'] = array(
    '#type' => 'textarea',
    '#title' => t('Receipt footer'),
    '#default_value' => variable_get('uberpos_receipt_footer_body', ''),
    '#rows' => 10,
    '#weight' => -2,
  );

  $form['receipt_footer_field_format'] = filter_form(variable_get('uberpos_receipt_footer_format', FILTER_FORMAT_DEFAULT), -1, array('receipt_footer_field_format'));

  $form['search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

function uberpos_settings_form_submit($form, &$form_state) {
  variable_set("uberpos_show_help", $form_state['values']['show_help']);
  variable_set("uberpos_welcome_body", $form_state['values']['welcome_body']);
  variable_set("uberpos_welcome_format", $form_state['values']['welcome_format']);
  variable_set("uberpos_receipt_header_body", $form_state['values']['receipt_header_field']);
  variable_set("uberpos_receipt_header_format", $form_state['values']['receipt_header_field_format']);
  variable_set("uberpos_receipt_footer_body", $form_state['values']['receipt_footer_field']);
  variable_set("uberpos_receipt_footer_format", $form_state['values']['receipt_footer_field_format']);
}

