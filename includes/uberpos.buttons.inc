<?php

/**
 * @file
 * Contains a hook to define the default UberPOS buttons.
 */

function uberpos_uberpos_buttons() {
  $i = 0;

  while ($i++ < 9) {
    $buttons[] = array(
      'section' => 'number-pad',
      'text' => $i,
      'weight' => $i,
      'attributes' => array(
        'class' => 'uberpos-button-'. $i .' up-button',
      ),
    );
  }

  $buttons[] = array(
    'section' => 'number-pad',
    'text' => '0',
    'weight' => 10,
    'attributes' => array(
      'class' => 'uberpos-button-0 up-button',
    ),
  );

  $buttons[] = array(
    'section' => 'number-pad',
    'text' => '00',
    'weight' => 11,
    'attributes' => array(
      'class' => 'uberpos-button-00 up-button',
    ),
  );

  $buttons[] = array(
    'section' => 'number-pad',
    'text' => t('*'),
    'weight' => 12,
    'attributes' => array(
      'class' => 'uberpos-button-asterisk up-button',
    ),
    'help' => 'command.asterisk',
  );

  $buttons[] = array(
    'section' => 'left',
    'text' => t('ENTER'),
    'weight' => 1,
    'attributes' => array(
      'onclick' => 'uberpos_add_text_submit(""); return false;',
      'class' => 'uberpos-button-enter up-button-submit',
    ),
  );

  $buttons[] = array(
    'section' => 'left',
    'text' => t('DEPT.'),
    'weight' => 5,
    'attributes' => array(
      'onclick' => "uberpos_add_text('DP'); return false;",
      'class' => 'uberpos-button-dept up-button-submit',
    ),
    'help' => 'command.dp',
  );

  $buttons[] = array(
    'section' => 'left',
    'text' => t('CASH'),
    'weight' => 10,
    'attributes' => array(
      'onclick' => "uberpos_add_text_submit('CA'); return false;",
      'class' => 'uberpos-button-cash up-button-submit',
    ),
    'help' => 'command.ca',
  );

  $buttons[] = array(
    'section' => 'left',
    'text' => t('CREDIT'),
    'weight' => 15,
    'attributes' => array(
      'onclick' => "uberpos_add_text_submit('CC'); return false;",
      'class' => 'uberpos-button-credit up-button-submit',
    ),
    'help' => 'command.cc',
  );

  $buttons[] = array(
    'section' => 'left',
    'text' => t('CHECK'),
    'weight' => 20,
    'attributes' => array(
      'onclick' => "uberpos_add_text_submit('CK'); return false;",
      'class' => 'uberpos-button-check up-button-submit',
    ),
    'help' => 'command.ck',
  );

  $buttons[] = array(
    'section' => 'right',
    'text' => t('VOID'),
    'weight' => 5,
    'attributes' => array(
      'onclick' => "uberpos_add_text_submit('VD'); return false;",
      'class' => 'uberpos-button-void up-button-submit',
    ),
    'help' => 'command.vd',
  );

  $buttons[] = array(
    'section' => 'right',
    'text' => t('REFUND'),
    'weight' => 10,
    'attributes' => array(
      'onclick' => "uberpos_add_text_submit('RF'); return false;",
      'class' => 'uberpos-button-refund up-button-submit',
    ),
    'help' => 'command.rf',
  );

  $buttons[] = array(
    'section' => 'right',
    'text' => t('ORDER'),
    'weight' => 15,
    'attributes' => array(
      'onclick' => "uberpos_add_text_submit('OR'); return false;",
      'class' => 'uberpos-button-order up-button-submit',
    ),
    'help' => 'command.or',
  );

  $buttons[] = array(
    'section' => 'right',
    'text' => t('MEMBER'),
    'weight' => 20,
    'attributes' => array(
      'onclick' => "uberpos_add_text_submit('ID'); return false;",
      'class' => 'uberpos-button-member up-button-submit',
    ),
    'help' => 'command.id',
  );

  $buttons[] = array(
    'section' => 'right',
    'text' => t('UP'),
    'weight' => 25,
    'attributes' => array(
      'onclick' => 'Drupal.Uberpos.keyUp(); return false;',
      'class' => 'uberpos-button-up up-button-submit',
    ),
  );

  $buttons[] = array(
    'section' => 'right',
    'text' => t('DOWN'),
    'weight' => 30,
    'attributes' => array(
      'onclick' => 'Drupal.Uberpos.keyDown(); return false;',
      'class' => 'uberpos-button-down up-button-submit',
    ),
  );

  $buttons[] = array(
    'section' => 'right',
    'text' => t('CLEAR'),
    'weight' => 35,
    'attributes' => array(
      'onclick' => "uberpos_add_text_submit('CL'); return false;",
      'class' => 'uberpos-button-clear up-button-submit',
    ),
    'help' => 'command.cl',
  );

  $buttons[] = array(
    'section' => 'right',
    'text' => t('CANCEL'),
    'weight' => 40,
    'attributes' => array(
      'onclick' => "uberpos_add_text_submit('CN'); return false;",
      'class' => 'uberpos-button-cancel up-button-submit',
    ),
    'help' => 'command.cn',
  );

  $buttons[] = array(
    'section' => 'right',
    'text' => t('LOGOUT'),
    'weight' => 45,
    'attributes' => array(
      'onclick' => "uberpos_add_text_submit('SO'); return false;",
      'class' => 'uberpos-button-logout up-button-submit',
    ),
  );

  return $buttons;
}


