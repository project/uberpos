<?php

/**
 * @file
 * Contains a hook to define the default UberPOS commands.
 */

function uberpos_uberpos_command() {
  $cmds = array('uberpos_command_void',
                'uberpos_command_clear',
                'uberpos_command_open_ring',
                'uberpos_command_accept_payment',
                'uberpos_command_reprint',
                'uberpos_command_signout',
                'uberpos_command_load_user',
                'uberpos_command_cancel',
                'uberpos_command_refund',
                'uberpos_command_load_order',
                'uberpos_command_closing_report',
  );
  return $cmds;
}

function uberpos_command_cancel(&$order, $item, $input, &$js) {
  if (isset($order) && $input == 'CN') {
    uc_order_update_status($order->order_id, 'pos_canceled');

    $result = db_query('SELECT * FROM {uc_order_products} WHERE order_id = %d', $order->order_id);

    /* Pull the trigger to remove all items in this order. This is useful for
     * keeping track of stock.
     */
    while ($row = db_fetch_object($result)) {
      ca_pull_trigger('uberpos_item_removed', $order, $row);
    }

    uberpos_clear_message(t('Order Canceled'));
    $order = NULL;
    return 'CN';
  }
}

function uberpos_command_clear(&$order, $item, $input, &$js) {
  if ($input == 'CL') {
    $order = NULL;
    return 'CL';
  }
}

function uberpos_command_signout(&$order, $item, $input, &$js) {
  if ($input == 'SO') {
    $js['redirect'] = url('logout', array('query' => array('destination' => 'pos-login')));
    return 'SO';
  }
}

function uberpos_command_reprint(&$order, $item, $input, &$js) {
  if ($order && $input == 'RP') {
    $times_printed = uberpos_receipt_times_printed($order->order_id);

    if ($times_printed > 0) {
      $_SESSION['uberpos_reprint'] = TRUE;
    }

    return 'RP';
  }
}

function uberpos_command_void(&$order, $item, $input, &$js) {
  if (!$order) {
    return FALSE;
  }

  if ($input == 'VD') {
    $parsed_item = uberpos_parse_item($item);

    if ($parsed_item['type'] == 'IT') {
      $result = db_query('SELECT * FROM {uc_order_products} WHERE order_product_id = %d', $parsed_item['item_no']);

      $item_info = db_fetch_object($result);

      db_query('INSERT INTO {up_log} (order_id, order_product_id, command, price, title, model, time) VALUES (%d, %d, \'VD\', %f, \'%s\', \'%s\', %d)', $item_info->order_id, $item_info->order_product_id, $item_info->price, $item_info->title, $item_info->model, time());

      db_query('DELETE FROM {uc_order_products} WHERE order_product_id = %d', $parsed_item['item_no']);

      ca_pull_trigger('uberpos_item_removed', $order, $item_info);

      $order = uc_order_load($order->order_id);
    }
    elseif ($parsed_item['type'] == 'PA') {
      uc_payment_delete($parsed_item['item_no']);
    }
    else {
      uberpos_clear_message(t('Cannot void this line.'));
    }

    return 'VD';
  }
}

function uberpos_command_load_order(&$order, $item, $input, &$js) {
  $order_id = drupal_substr($input, 0, drupal_strlen($input) - 2);
  $cmd = drupal_substr($input, drupal_strlen($input) - 2, 2);

  if ($cmd == 'OR' && is_numeric($order_id)) {
    if (isset($order)) {
      uberpos_clear_message(t('Please clear the current order before loading a new one.'));
      return 'OR';
    }
    else {
      $order = uc_order_load($order_id);

      if (!$order) {
        uberpos_clear_message(t('Invalid order number'));
        return 'OR';
      }

      if (!uberpos_is_pos_order($order->order_status)) {
        uberpos_clear_message(t('This order is not a POS order'));
        $order = NULL;
        return 'OR';
      }

      return 'loaded';
    }
  }
}

/* Loads up a specified user and sets them as the order's customer. */
function uberpos_command_load_user(&$order, $item, $input, &$js) {
  $uid = drupal_substr($input, 0, drupal_strlen($input) - 2);
  $cmd = drupal_substr($input, drupal_strlen($input) - 2, 2);

  if ($cmd == 'ID') {
    // TODO: use query for speed
    $user = user_load(array('uid' => $uid));

    if ($user !== FALSE) {
      $order->uid = $user->uid;
    }
    else {
      uberpos_clear_message(t('Invalid user ID'));
    }
    return 'ID';
  }
}

function uberpos_command_accept_payment(&$order, $item, $input, &$js) {
  global $user;
  $amount = drupal_substr($input, 0, drupal_strlen($input) - 2);
  $type = drupal_substr($input, drupal_strlen($input) - 2, 2);

  if ($type == 'CA' || $type == 'CK' || $type == 'CC') {
    if ($type == 'CC' && variable_get('up_authorizenet_enabled', 0) == 1 && module_exists('up_authorizenet')) {
      $_SESSION['uberpos_interpreter'] = 'up_authorizenet_cc_interpreter';
      uberpos_clear_message(t('Please swipe the credit card'));
      return 'CC'; 
    }

    if (drupal_strlen($input) != 2 && !is_numeric($amount)) {
      uberpos_clear_message(t('You may only enter numbers to the left of the payment type'));
      return $type;
    }

    $balance = uc_payment_balance($order);
    if (drupal_strlen($input) == 2) {
      // If they don't enter in an amount, default to the total owed.
      $prec = variable_get('uc_currency_prec', 2);
      $amount = round($balance, $prec);
    }
    else {
      $amount = $amount * .01;
      if ($amount > $balance && $order->order_status != 'pos_refund_in_progress') {
        if ($type !='CA') {
            uberpos_clear_message(t('No change can be given for this payment type.'));
            return $type;
        }
        else {
          $change_due = $amount - $balance;
          $amount = $balance;
        }
      }
    }

    if ($balance > 0 || ($balance < 0 && $order->order_status == 'pos_refund_in_progress')) {

      if ($type == 'CK') {
        uc_payment_enter($order->order_id, 'pos_check', $amount, $order->uid, NULL, t('UberPOS !type', array('!type' => $type)));
      }
      elseif ($type == 'CA') {
        $data['change'] = $change_due;
        uc_payment_enter($order->order_id, 'pos_cash', $amount, $order->uid, $data, t('UberPOS !type', array('!type' => $type)));
      }
      elseif ($type == 'CC') {
        /* Use UberPOS's basic CC payment method */
        uc_payment_enter($order->order_id, 'pos_credit', $amount, $order->uid, NULL, t('UberPOS !type', array('!type' => $type)));
      }

      /* Refresh the order */
      uc_order_save($order);
      $order = uc_order_load($order->order_id);

      $due = uc_payment_balance($order);

      $prec = variable_get('uc_currency_prec', 2);
      $due = round($due, $prec); /* We were getting $0.003 remaining for example. */

      if ($order->order_total > 0 && $due <= 0) {
        if ($due < 0) {
          uc_payment_enter($order->order_id, 'pos_cash', $due, $order->uid, array('change' => TRUE), t('UberPOS CA Change'));
        }
        uc_order_update_status($order->order_id, 'pos_completed');
        $order = uc_order_load($order->order_id);
        if ($change_due) {
          uberpos_clear_message(t('Thank you for shopping!<br />Change Due: ' . uc_price($change_due, array()), array('clear_order' => TRUE)));
        }
        else {
          uberpos_clear_message(t('Thank you for shopping!'), array('clear_order' => TRUE));
        }
      }
    }
    else {
      uberpos_clear_message(t('This order is already paid in full.'));
    }
    return $type;
  }
}

function uberpos_parse_item($item) {
  $item_no = drupal_substr($item, 2, drupal_strlen($item));
  $type = drupal_substr($item, 0, 2);

  return array('item_no' => $item_no, 'type' => $type);
}

function uberpos_command_refund(&$order, $item, $input, &$js) {
  if ($input == 'RF') {
    $parsed_item = uberpos_parse_item($item);

    if ($parsed_item['type'] != 'IT') {
      uberpos_clear_message(t('Cannot refund this line.'));
      return 'RF';
    }

    $result = db_query('SELECT * FROM {uc_order_products} WHERE order_product_id = %d', $parsed_item['item_no']);

    $item_info = db_fetch_object($result);

    if ($item_info->price < 0) {
      uberpos_clear_message(t('Invalid command entered.'));
      return $order;
    }

    $orig_order_total = $order->order_total;
    db_query('DELETE FROM {uc_order_products} WHERE order_product_id = %d', $parsed_item['item_no']);

    /* TODO: Clean up. This is a really bad way to reload an order */
    $order = uc_order_load($order->order_id);
    uc_order_update_status($order->order_id, 'pos_refund_in_progress');
    uc_order_save($order);
    $order = uc_order_load($order->order_id);

    $refund = $orig_order_total - $order->order_total;

    db_query('INSERT INTO {up_log} (order_id, order_product_id, command, price, title, model, time) VALUES (%d, %d, \'RF\', %f, \'%s\', \'%s\', %d)', $item_info->order_id, $item_info->order_product_id, $refund, t('REFUND') .' '. $item_info->title, $item_info->model, time());

    ca_pull_trigger('uberpos_item_refunded', $order, $item_info);

    return 'RF';
  }
}

function uberpos_command_closing_report(&$order, $item, $input, &$js) {
  if ($input == 'MG') {
    $js['receipt'] = theme('uberpos_closing_report');
  }
  else {
    $date = drupal_substr($input, 0, drupal_strlen($input) - 2);
    $cmd = drupal_substr($input, drupal_strlen($input) - 2, 2);

    if ($cmd != 'MG') {
      return;
    }

    if (!is_numeric($date) || drupal_strlen($date) != 8) {
      uberpos_clear_message(t('To print a closing report for a past day, please enter the date in the format of "MMDDYYYY" followed by the "MG" command.'));
      return 'MG';
    }

    $date = str_split($date);

    $month = $date[0] . $date[1];
    $day = $date[2] . $date[3];
    $year = $date[4] . $date[5] . $date[6] . $date[7];

    $js['receipt'] = theme('uberpos_closing_report', $month, $day, $year);
  }

  return 'MG';
}

function uberpos_command_open_ring(&$order, $item, $input, &$js) {
  $dp = preg_split('/DP/', $input);

  if (is_numeric($dp[0]) && is_numeric($dp[1])) {
    $amount = $dp[0];
    $tid = $dp[1];

    $department = taxonomy_get_term($tid);

    if (!isset($department->name) || $department->vid != variable_get('uc_catalog_vid', 0)) {
      uberpos_clear_message(t('Invalid department number.'));
      return $order;
    }

    if (!$order) {
      $order = uberpos_add_order();
    }

    $product->qty = 1;
    $product->title = $department->name;
    $product->price = $amount * 0.01;

    module_invoke_all('cart_item', 'load', $product);
    drupal_alter('order_product', $product, $order);
    uc_order_product_save($order->order_id, $product);

    if (variable_get('uc_order_logging', TRUE)) {
      uc_order_log_changes($order->order_id, array('add' => 'Added ('. $product->qty .') '. $product->title .' to order.'));
    }

    $order = uc_order_load($order->order_id);
    return 'DP';
  }
}

function uberpos_command_upc(&$order, $input) {
  /* Handle UPCs and all other input */
  $split_arr = preg_split('/\*/', $input);

  if (isset($split_arr[0]) && isset($split_arr[1]) && is_numeric($split_arr[0])) {
    $qty = $split_arr[0];
    $sku = $split_arr[1];
  }
  else {
    $qty = 1;
    $sku = $input;
  }

  if (!empty($order) && $order->order_status != 'pos_in_progress') {
    uberpos_clear_message(t('No products can be added. This order is no longer in progress. If you were not trying to add a product, the command entered was not recognized.'));
    return "new";
  }

  $result = uberpos_add_product($sku, $order, $qty);
  if ($result && ($order->order_status == 'pos_in_progress' || empty($order->order_status))) {
    $order = $result;
  }
  else {
    uberpos_clear_message(t('Invalid UPC number or command entered'));
  }

  return 'new';
}

